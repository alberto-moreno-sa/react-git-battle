# React without Redux
Example of a simple web application with react using ECMAScript 6 

Getting Started
---------------

For installation instructions.

$ Download and install [NodeJs](https://nodejs.org/en/) 

You can see the final project [Here](https://battle-github-486b8.firebaseapp.com/)

```
Run in console:
$ npm install

For Develomemnt Server
$ npm start 

For build to productionm

$ npm run  build

For fire base 

- For login and init Firebase

& firebase-init

- For deploy

$ npm run deploy 
```